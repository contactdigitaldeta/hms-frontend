import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenstorageService } from 'src/Services/authentication/tokenstorage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {

  constructor(private router: Router,private Tokenservice:TokenstorageService) { }
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const token_key = this.Tokenservice.getToken();
      // if(!this.loginService.isLoggedIn()){
      //  this.router.navigate(['login']);
      // }
      // return true;
      if(token_key == null){
        this.router.navigate(['user-pages/register']);
      }
      return true;
    }
  
}
