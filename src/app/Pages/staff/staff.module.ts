import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllStaffsComponent } from './all-staffs/all-staffs.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const routes: Routes = [   
  { path:"All-staffs",component: AllStaffsComponent },
]

@NgModule({
  declarations: [AllStaffsComponent],
  imports: [
    RouterModule.forChild(routes),
    NgbModule,
    CommonModule
  ]
})
export class StaffModule { }
