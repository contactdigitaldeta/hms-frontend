import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomsComponent } from './rooms/rooms.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



const routes: Routes = [
  { path: 'All-Rooms', component: RoomsComponent },
]


@NgModule({
  declarations: [RoomsComponent],
  imports: [
    RouterModule.forChild(routes),
    NgbModule,
    CommonModule
  ]
})


export class RoomModule { }
