import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BookingService } from 'src/Services/Booking/booking.service';

@Component({
  selector: 'app-all-bookings',
  templateUrl: './all-bookings.component.html',
  styleUrls: ['./all-bookings.component.scss']
})
export class AllBookingsComponent implements OnInit {


  title = 'google-places-autocomplete';
  userAddress: string = ''
  userLatitude: string = ''
  userLongitude: string = ''

  formattedAddress= '';
  

  bookingsForm = new FormGroup({
    name : new FormControl(''),
    mobileNo: new FormControl(''),
    checkIn: new FormControl(''),
    checkOut: new FormControl(''),
    emailId : new FormControl(''),
    gender : new FormControl(''),
    roomNo: new FormControl(''),
    amount: new FormControl(''),
    payment: new FormControl(''),

  });

  constructor(private modalService: NgbModal,private bookService: BookingService) { }

  ngOnInit(): void {
  }

  booking(){
    console.log('booking data ' + JSON.stringify(this.bookingsForm.value));
    this.bookService.addGuest(this.bookingsForm.value).subscribe(data => {
      console.log(data);
    })

  }


  public handleAddressChange(address: any) {

        this.userAddress = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    }
  

  openMediumModal( mediumModalContent ) {
    this.modalService.open( mediumModalContent );
  }

  open(focusFirst) {
    this.modalService.open(focusFirst);
  }
  

}
