import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllBookingsComponent } from './all-bookings/all-bookings.component';
import { AddBookingsComponent } from './add-bookings/add-bookings.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

const routes: Routes = [
  { path: 'All-Bookings', component: AllBookingsComponent },
  {path: 'Add-Bookings', component: AddBookingsComponent },
  // {path: 'Details-Bookings', component: DetailsBookingComponent },
]



@NgModule({
  declarations: [AllBookingsComponent, AddBookingsComponent],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgbModule,
    GooglePlaceModule
  ]
})
export class BookingsModule { }
