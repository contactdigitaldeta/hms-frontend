import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonsComponent } from './buttons/buttons.component';
import { DropdownsComponent } from './dropdowns/dropdowns.component';
import { ModalsComponent } from './modals/modals.component';

const routes: Routes = [
  { path: 'buttons', component: ButtonsComponent },
  { path: 'dropdowns', component: DropdownsComponent },
  { path: 'modals', component: ModalsComponent },
];

@NgModule({
  declarations: [ButtonsComponent, DropdownsComponent, ModalsComponent],
  imports: [
    CommonModule,
    NgbModule,
    RouterModule.forChild(routes),
  ]
})
export class BasicUiModule { }
