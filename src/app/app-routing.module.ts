import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './user-pages/login/login.component';
import { RegisterComponent } from './user-pages/register/register.component';


const routes: Routes = [
  { path: '', redirectTo: '/user-pages/register', pathMatch: 'full' },
  {path:'user-pages/register',component: RegisterComponent},
  {path:'user-pages/login',component:LoginComponent},
  { path: 'dashboard', component: DashboardComponent },
  { path: 'bookings', loadChildren: () => import('./Pages/bookings/bookings.module').then(m => m.BookingsModule) },
  { path: 'Rooms', loadChildren: () => import('./Pages/room/room.module').then(m => m.RoomModule) },
  { path: 'staff', loadChildren: () => import('./Pages/staff/staff.module').then(m => m.StaffModule) },
  // { path: 'basic-ui', loadChildren: () => import('./basic-ui/basic-ui.module').then(m => m.BasicUiModule) },
  // { path: 'forms', loadChildren: () => import('./forms/form.module').then(m => m.FormModule) },
  // { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule) },
  { path: 'apps', loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule) },
  { path: 'user-pages', loadChildren: () => import('./user-pages/user-pages.module').then(m => m.UserPagesModule) },
  { path: 'error-pages', loadChildren: () => import('./error-pages/error-pages.module').then(m => m.ErrorPagesModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
