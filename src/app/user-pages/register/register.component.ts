import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { data } from 'jquery';
import { LoginService } from 'src/Services/authentication/login.service';
import { TokenstorageService } from 'src/Services/authentication/tokenstorage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  errorMessage: '';
  isLoginFailed = true;
  constructor(private loginservice: LoginService,private tokenservice:TokenstorageService,private router:Router) { }

  registerForm = new FormGroup({
    username: new FormControl('test'),
    password: new FormControl('test')
  });

  signin(){
    console.log(JSON.stringify(this.registerForm.value));
    this.loginservice.Login(this.registerForm.value).subscribe(data =>{
      this.tokenservice.saveToken(data.accessToken);
      this.tokenservice.saveUser(data);
    const token_key = this.tokenservice.getToken();
    if(token_key != null){
      this.router.navigate(['/dashboard']) ;  
    } else{
      this.reloadPage();
    }
    // else{
    //   this.isLoggedIn=false;
    //   this.reloadPage();
    // }
    this.isLoginFailed = false;
    // this.roles = this.Tokenservice.getUser().roles;
    // this.reloadPage();
   
  },
  err => {
    this.errorMessage = err.error.message;
    this.isLoginFailed = true;
  }
    );
  }

  reloadPage(){
    window.location.reload();
  }

  ngOnInit():void {
  }

}
