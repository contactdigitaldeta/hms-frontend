import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenstorageService } from './tokenstorage.service';



const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}


@Injectable({
  providedIn: 'root'
})

export class LoginService {

  private readonly JWT_TOKEN='JWT_TOKEN'

  private hmsHost = `${environment.host}`;
  private hmsPort = `${environment.port}`;
  private hmsProtocol = `${environment.protocol}`;
  private hmsUrl = '';

  constructor(
    private http:HttpClient,
    private tokenservice:TokenstorageService
  ) { 
    this.hmsUrl = this.hmsProtocol+'//'+this.hmsHost+':'+this.hmsPort+'/HMS/api/auth';
  }




  
  Login(logindata): Observable<any>{
    // const url_before = 'api/auth';
    const url = `${this.hmsUrl}/signin`;
    return this.http.post(url, logindata, httpOptions);
   }


   register(signupdata): Observable<any> {
    const url = `${this.hmsUrl}/signup`;
    return this.http.post(url , signupdata, httpOptions);
  }

  isLoggedIn(){
    let token = this.tokenservice.getToken();
    console.log("token "+ token)
    if(token !== null){
      return true;
    } else
     return false;
  }
  
  getJwtToken(){
    return localStorage.getItem(this.JWT_TOKEN);
  }
}
