import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private hmsHost = `${environment.host}`;
  private hmsPort = `${environment.port}`;
  private hmsProtocol = `${environment.protocol}`;
  private hmsUrl = '';

  constructor(private http: HttpClient) { 
    this.hmsUrl = this.hmsProtocol+'//'+this.hmsHost+':'+this.hmsPort+'/HMS';
  }




  getPublicContent(): Observable<any> {
    const url = `${this.hmsUrl}/all`;
    return this.http.get(url, { responseType: 'text' });
  }
  getSupervisorBoard(): Observable<any> {
    const url = `${this.hmsUrl}/supervisor`;
    return this.http.get(url, { responseType: 'text' });
  }
  getAdminBoard(): Observable<any> {
    const url = `${this.hmsUrl}/admin`;
      return this.http.get(url, { responseType: 'text' });
    }
}
