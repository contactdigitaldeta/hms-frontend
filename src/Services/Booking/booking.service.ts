import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';



const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})

export class BookingService {

  private hmsHost = `${environment.host}`;
  private hmsPort = `${environment.port}`;
  private hmsProtocol = `${environment.protocol}`;
  private hmsUrl = '';

  constructor(
    private http:HttpClient,
  ) { 
    this.hmsUrl = this.hmsProtocol+'//'+this.hmsHost+':'+this.hmsPort+'/HMS';
  }

  addGuest(Booking): Observable<any> {
    const url = `${this.hmsUrl}/guest`;
    return this.http.post(url, Booking, httpOptions);
  }

}
